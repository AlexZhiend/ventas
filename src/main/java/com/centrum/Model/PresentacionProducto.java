package com.centrum.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "presentacionproducto")
public class PresentacionProducto {
	 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idpresentacion;
	
	@Column
	private String nombre;
	
	@Column
	private String cantidad;
	
	@Column
	private String unidad;

	public int getIdpresentacion() {
		return idpresentacion;
	}

	public void setIdpresentacion(int idpresentacion) {
		this.idpresentacion = idpresentacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

}
