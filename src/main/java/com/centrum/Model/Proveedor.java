package com.centrum.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "proveedor")
public class Proveedor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idproveedor;
	
	@Column(name = "nombreproveedor", nullable=false)
	private String nombreproveedor;
	
	@Column(name = "descripcionproveedor", nullable=false)
	private String descripcionproveedor;
	
	@Column(name = "telefonoproveedor", nullable=false)
	private String telefonoproveedor;
	
	@Column(name = "direccionproveedor", nullable=false)
	private String direccionproveedor;
	
	@Column(name = "correoproveedor")
	private String correoproveedor;
  
	public int getIdproveedor() {
		return idproveedor;
	}

	public void setIdproveedor(int idproveedor) {
		this.idproveedor = idproveedor;
	}

	public String getNombreproveedor() {
		return nombreproveedor;
	}

	public void setNombreproveedor(String nombreproveedor) {
		this.nombreproveedor = nombreproveedor;
	}

	public String getDesripcionproveedor() {
		return descripcionproveedor;
	}

	public void setDesripcionproveedor(String desripcionproveedor) {
		this.descripcionproveedor = desripcionproveedor;
	}

	public String getTelefonoproveedor() {
		return telefonoproveedor;
	}

	public void setTelefonoproveedor(String telefonoproveedor) {
		this.telefonoproveedor = telefonoproveedor;
	}

	public String getDireccionproveedor() {
		return direccionproveedor;
	}

	public void setDireccionproveedor(String direccionproveedor) {
		this.direccionproveedor = direccionproveedor;
	}

	public String getCorreoproveedor() {
		return correoproveedor;
	}

	public void setCorreoproveedor(String correoproveedor) {
		this.correoproveedor = correoproveedor;
	}

	public Proveedor(int idproveedor, String nombreproveedor, String desripcionproveedor, String telefonoproveedor,
			String direccionproveedor, String correoproveedor) {
		super();
		this.idproveedor = idproveedor;
		this.nombreproveedor = nombreproveedor;
		this.descripcionproveedor = desripcionproveedor;
		this.telefonoproveedor = telefonoproveedor;
		this.direccionproveedor = direccionproveedor;
		this.correoproveedor = correoproveedor;
	}

	public Proveedor() {
		super();
	} 
	
	
	
}
