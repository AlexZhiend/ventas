package com.centrum.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "tipocomprobante")
public class TipoComprobante {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idtipocomprobante;

	@Column
	private String denominacion;

	public int getIdtipocomprobante() {
		return idtipocomprobante;
	}

	public void setIdtipocomprobante(int idtipocomprobante) {
		this.idtipocomprobante = idtipocomprobante;
	}

	public String getDenominacion() {
		return denominacion;
	}

	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	
	
	
}
