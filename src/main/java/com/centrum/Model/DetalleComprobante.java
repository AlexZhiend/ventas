package com.centrum.Model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "detallecomprobante")
public class DetalleComprobante {
	 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int iddetalleComprobante;

	@Column
	private int cantidad;
	
	@ManyToOne
	@JoinColumn(name="idproducto", nullable=false)
	private Producto producto;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="idcomprobante", nullable=false)
	private Comprobante comprobante;
	

	public int getIddetalleComprobante() {
		return iddetalleComprobante;
	}

	public void setIddetalleComprobante(int iddetalleComprobante) {
		this.iddetalleComprobante = iddetalleComprobante;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Comprobante getComprobante() {
		return comprobante;
	}

	public void setComprobante(Comprobante comprobante) {
		this.comprobante = comprobante;
	}
		
	
}
