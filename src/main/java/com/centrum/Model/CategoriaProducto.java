package com.centrum.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "categoriaproducto")
public class CategoriaProducto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idcategoriaproducto;
	
	@Column(name = "nombrecategoriaproducto", nullable= false)
	private String nombrecategoriaproducto;

	public int getIdcategoriaproducto() {
		return idcategoriaproducto;
	}

	public void setIdcategoriaproducto(int idcategoriaproducto) {
		this.idcategoriaproducto = idcategoriaproducto;
	}

	public String getNombrecategoriaproducto() {
		return nombrecategoriaproducto;
	}

	public void setNombrecategoriaproducto(String nombrecategoriaproducto) {
		this.nombrecategoriaproducto = nombrecategoriaproducto;
	}

	public CategoriaProducto(int idcategoriaproducto, String nombrecategoriaproducto) {
		super();
		this.idcategoriaproducto = idcategoriaproducto;
		this.nombrecategoriaproducto = nombrecategoriaproducto;
	}

	public CategoriaProducto() {
		super();
	}

	
	
}
