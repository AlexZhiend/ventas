package com.centrum.Model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;


//
//@SqlResultSetMappings({
//	@SqlResultSetMapping(
//	    name = "ReporteProductoGeneral",
//	    classes = @ConstructorResult(
//	            targetClass = ProductoReport.class,
//	            columns = {
//	            		@ColumnResult(name = "idproducto"),
//	                    @ColumnResult(name = "cantidadproducto"),
//	                    @ColumnResult(name = "fvproducto"),
//	                    @ColumnResult(name = "nombreproducto"),
//	                    @ColumnResult(name = "marcaproducto"),
//	                    @ColumnResult(name = "loteproducto"),
//	                    @ColumnResult(name = "pingresoproducto"),
//	                    @ColumnResult(name = "pventaproducto"),
//	                    @ColumnResult(name = "unidadpresentacionproducto"),
//	                    @ColumnResult(name = "cantidadpresentacionproducto"),
//	                    @ColumnResult(name = "nombrepresentacionproducto")
//	            }
//	    		)
//	   ),
//	@SqlResultSetMapping(
//		    name = "ReporteProductoVenta",
//		    classes = @ConstructorResult(
//		            targetClass = ProductoVentasReport.class,
//		            columns = {
//		            		@ColumnResult(name = "idproducto"),
//		                    @ColumnResult(name = "nombreproducto"),
//		                    @ColumnResult(name = "nombrepresentacionproducto"),
//		                    @ColumnResult(name = "cantidadpresentacionproducto"),
//		                    @ColumnResult(name = "unidadpresentacionproducto"),
//		                    @ColumnResult(name = "pventaproducto"),
//		                    @ColumnResult(name = "cantidadtotal"),
//		                    @ColumnResult(name = "total")
//		            }
//		    		)
//		   )
//		}
//	)
//
//@NamedNativeQueries({
//	@NamedNativeQuery(name = "ReporteProductoGeneral",resultClass = ProductoReport.class, 
//			query = "select pr.idproducto,pr.cantidadproducto,pr.fvproducto,pr.nombreproducto,pr.marcaproducto,pr.loteproducto,\r\n" + 
//					"pr.pingresoproducto,pr.pventaproducto,pp.nombrepresentacionproducto,pp.unidadpresentacionproducto,\r\n" + 
//					"pp.cantidadpresentacionproducto\r\n" + 
//					"from producto pr inner join presentacionproducto pp on pp.idpresentacionproducto = pr.idpresentacionproducto"
//			),
//	@NamedNativeQuery(name = "ReporteProductoVenta",resultClass = ProductoVentasReport.class, 
//	query = "select pro.idproducto,pro.nombreproducto,pre.nombrepresentacionproducto,pre.cantidadpresentacionproducto,\r\n" + 
//			"pre.unidadpresentacionproducto,pro.pventaproducto,sum(det.cantidad) as cantidadtotal,\r\n" + 
//			"round( CAST(sum(pro.pventaproducto * det.cantidad) as numeric), 2) as total\r\n" + 
//			"from producto pro inner join detalleordenf det on det.idproducto = pro.idproducto\r\n" + 
//			"inner join presentacionproducto pre on pre.idpresentacionproducto = pro.idpresentacionproducto\r\n" + 
//			"inner join ordenfarmacia od on od.idordenfarmacia = det.idordenfarmacia\r\n" + 
//			"where fechaordenfarmacia BETWEEN TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY') \r\n" + 
//			"GROUP by pro.idproducto, pre.nombrepresentacionproducto,pre.cantidadpresentacionproducto,\r\n" + 
//			"pre.unidadpresentacionproducto"
//	)
//})


@Entity
@Table(name = "producto")
public class Producto {
	
	@Id
	@Column(name = "idproducto")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idproducto;
	
	@Column
	private String codigo;
	
	@Column(name = "nombreproducto", nullable = false)
	private String nombreproducto;
	
	@JsonSerialize(using = ToStringSerializer.class)
	@Column(name = "fvproducto")
	private LocalDate fvproducto;
	
	@Column(name = "cantidadproducto", nullable=false)
	private int cantidadproducto;
	
	@Column(name = "pventaproducto", nullable =false)
	private double pventaproducto;
	
	@Column(name = "pingresoproducto", nullable=false)
	private double pingresoproducto;
	
	@Column(name = "marcaproducto", nullable=false)
	private String marcaproducto;
	
	@JsonSerialize(using = ToStringSerializer.class)
	@Column(name = "fingresoproducto")
	private LocalDate fingresoproducto; 
	
	@OneToOne
	@JoinColumn(name = "idpresentacionproducto", nullable=false)
	private PresentacionProducto presentacionproducto;
	
	@OneToOne
	@JoinColumn(name = "idproveedor", nullable =false)
	private Proveedor proveedor;
	
	@OneToOne
	@JoinColumn(name = "idcategoriaproducto", nullable =false)
	private CategoriaProducto categoriaproducto;

	public int getIdproducto() {
		return idproducto;
	}

	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}

	public String getNombreproducto() {
		return nombreproducto;
	}

	public void setNombreproducto(String nombreproducto) {
		this.nombreproducto = nombreproducto;
	}

	public LocalDate getFvproducto() {
		return fvproducto;
	}

	public void setFvproducto(LocalDate fvproducto) {
		this.fvproducto = fvproducto;
	}

	public int getCantidadproducto() {
		return cantidadproducto;
	}

	public void setCantidadproducto(int cantidadproducto) {
		this.cantidadproducto = cantidadproducto;
	}

	public double getPventaproducto() {
		return pventaproducto;
	}

	public void setPventaproducto(double pventaproducto) {
		this.pventaproducto = pventaproducto;
	}

	public double getPingresoproducto() {
		return pingresoproducto;
	}

	public void setPingresoproducto(double pingresoproducto) {
		this.pingresoproducto = pingresoproducto;
	}

	public String getMarcaproducto() {
		return marcaproducto;
	}

	public void setMarcaproducto(String marcaproducto) {
		this.marcaproducto = marcaproducto;
	}

	public LocalDate getFingresoproducto() {
		return fingresoproducto;
	}

	public void setFingresoproducto(LocalDate fingresoproducto) {
		this.fingresoproducto = fingresoproducto;
	}

	public PresentacionProducto getPresentacionproducto() {
		return presentacionproducto;
	}

	public void setPresentacionproducto(PresentacionProducto presentacionproducto) {
		this.presentacionproducto = presentacionproducto;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public CategoriaProducto getCategoriaproducto() {
		return categoriaproducto;
	}

	public void setCategoriaproducto(CategoriaProducto categoriaproducto) {
		this.categoriaproducto = categoriaproducto;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	

}
