package com.centrum.Model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

//
//@SqlResultSetMappings({
//@SqlResultSetMapping(
//	    name = "ReporteOrdenFGeneral",
//	    classes = @ConstructorResult(
//	            targetClass = OrdenFReport.class,
//	            columns = {
//	            		@ColumnResult(name = "id"),
//	                    @ColumnResult(name = "fechaordenfarmacia"),
//	                    @ColumnResult(name = "consumidorordenfarmacia"),
//	                    @ColumnResult(name = "numeroorden"),
//	                    @ColumnResult(name = "rucordenfarmacia"),
//	                    @ColumnResult(name = "pventaproducto"),
//	                    @ColumnResult(name = "nombreproducto"),
//	                    @ColumnResult(name = "cantidad"),
//	                    @ColumnResult(name = "total"),
//	                    @ColumnResult(name = "descuento")
//	            }
//	    )
//	),
//@SqlResultSetMapping(
//		name = "TotalFarmaciaxDia",
//		classes = @ConstructorResult(
//				targetClass = TotalxDiaFarmacia.class,
//				columns = {
//	                    @ColumnResult(name = "totalxdia"),
//	                    @ColumnResult(name = "fechaordenfarmacia")
//				}
//			)
//		),
//@SqlResultSetMapping(
//	    name = "ReporteOrdenFDetallado",
//	    classes = @ConstructorResult(
//	            targetClass = DetalladoVentaF.class,
//	            columns = {
//	            		@ColumnResult(name = "id"),
//	                    @ColumnResult(name = "fechaordenfarmacia"),
//	                    @ColumnResult(name = "consumidorordenfarmacia"),
//	                    @ColumnResult(name = "numeroorden"),
//	                    @ColumnResult(name = "rucordenfarmacia"),
//	                    @ColumnResult(name = "pventaproducto"),
//	                    @ColumnResult(name = "nombreproducto"),
//	                    @ColumnResult(name = "cantidad"),
//	                    @ColumnResult(name = "total"),
//	                    @ColumnResult(name = "descuento"),
//	                    @ColumnResult(name = "estado")
//	            }
//	    )
//	),
//@SqlResultSetMapping(
//	    name = "NotaCreditoDetallado",
//	    classes = @ConstructorResult(
//	            targetClass = NCreditoFarmaciaReport.class,
//	            columns = {
//	            		@ColumnResult(name = "idordenfarmacia"),
//	            		@ColumnResult(name = "numeroorden"),
//	                    @ColumnResult(name = "tipo"),
//	                    @ColumnResult(name = "serie"),
//	                    @ColumnResult(name = "consumidorordenfarmacia"),
//	                    @ColumnResult(name = "exonera"),
//	                    @ColumnResult(name = "inafecto"),
//	                    @ColumnResult(name = "total"),
//	                    @ColumnResult(name = "fechaordenfarmacia")
//	            }
//	    )
//	)
//})
//
//@NamedNativeQueries({
//	@NamedNativeQuery(name = "ReporteOrdenFPaciente",resultClass = OrdenFReport.class, 
//			query = "select det.iddetalleordenf as id,od.fechaordenfarmacia,od.consumidorordenfarmacia,od.numeroorden,od.rucordenfarmacia,\r\n" + 
//					"pro.pventaproducto,pro.nombreproducto,det.cantidad,\r\n" + 
//					"round( CAST(sum(pro.pventaproducto * det.cantidad) as numeric), 2) as importe,round( CAST(sum(od.total) as numeric), 2) as total, od.descuento\r\n" + 
//					"from ordenfarmacia od inner join detalleordenf det on od.idordenfarmacia = det.idordenfarmacia\r\n" + 
//					"inner join producto pro on pro.idproducto = det.idproducto where od.numeroorden = :numeroorden\r\n" + 
//					"group by det.iddetalleordenf,od.fechaordenfarmacia,od.consumidorordenfarmacia,od.numeroorden,od.rucordenfarmacia,\r\n" + 
//					"pro.pventaproducto,pro.nombreproducto,det.cantidad,od.descuento,od.total"
//			),
//	@NamedNativeQuery(name = "TotalFarmaciaxDia",resultClass = TotalxDiaFarmacia.class, 
//	query = "select sum(od.total) as totalxdia,od.fechaordenfarmacia from ordenfarmacia od\r\n" + 
//			"where od.fechaordenfarmacia = TO_DATE(:fecha,'DD/MM/YYYY') and od.estado = 1 \r\n" + 
//			"group by od.fechaordenfarmacia"
//			),
//	
//	@NamedNativeQuery(name = "ReporteOrdenFDetallado",resultClass = DetalladoVentaF.class, 
//	query = "select det.iddetalleordenf as id,od.fechaordenfarmacia,od.consumidorordenfarmacia,od.numeroorden,od.rucordenfarmacia,\r\n" + 
//			"pro.pventaproducto,pro.nombreproducto,det.cantidad,od.estado,\r\n" + 
//			"round( CAST(sum(pro.pventaproducto * det.cantidad) as numeric), 2) as importe,round( CAST(sum(od.total) as numeric), 2) as total, od.descuento\r\n" + 
//			"from ordenfarmacia od inner join detalleordenf det on od.idordenfarmacia = det.idordenfarmacia \r\n" + 
//			"inner join producto pro on pro.idproducto = det.idproducto\r\n" + 
//			"where od.fechaordenfarmacia = TO_DATE(:fechaordenfarmacia,'DD/MM/YYYY')\r\n" + 
//			"group by det.iddetalleordenf,od.fechaordenfarmacia,od.consumidorordenfarmacia,od.numeroorden,od.rucordenfarmacia,\r\n" + 
//			"pro.pventaproducto,pro.nombreproducto,det.cantidad,od.descuento,od.total,od.estado"
//	),
//	@NamedNativeQuery(name = "NotaCreditoDetallado",resultClass = NCreditoFarmaciaReport.class, 
//	query = "select od.idordenfarmacia,od.fechaordenfarmacia, to_char( od.tipo, 'FM09') as tipo,to_char(od.serie, 'FM999099') as serie,to_char( od.tiporef, 'FM09') as tiporef,to_char( od.serieref, 'FM099') as serieref,\r\n" + 
//			"to_char( od.numeroref, 'FM099999') as numeroref, od.fechaordenref,od.totalref,\r\n" + 
//			" to_char( od.numeroorden, 'FM099999') as numeroorden,od.consumidorordenfarmacia,\r\n" + 
//			"(cast(od.descuento as DOUBLE PRECISION)/100 )*od.total as exonera, \r\n" + 
//			"od.total - (cast(od.descuento as DOUBLE PRECISION)/100)*od.total as inafecto, od.total\r\n" + 
//			"from ordenfarmacia od where od.fechaordenfarmacia between TO_DATE(:fechainicio,'DD/MM/YYYY') and TO_DATE(:fechafin,'DD/MM/YYYY') \r\n" + 
//			"and od.estado=1 \r\n" + 
//			"order by od.serie asc,od.tipo asc,od.numeroorden asc"
//			),
//})


@Entity
@Table(name = "comprobante")
public class Comprobante {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idcomprobante;
	
	@Column(name = "fecha")
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fecha;
	
	@Column 
	private int numero; 
	
	@Column
	private String ruc;
	
	@Column
	private String consumidor; 
	
	@Column
	private double total; 
	
	@Column
	private int descuento;
	
	@Column
	private int estado;
	
	@Column
	private int tipo;
	
	@Column
	private int serie;
	
	@Column
	private int serieref;
	
	@Column
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fecharef;
	
	@Column
	private int tiporef;
	
	@Column
	private int numeroref;
	
	@Column
	private double totalref;
	
	@OneToMany( mappedBy = "comprobante", cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REMOVE }, fetch = FetchType.LAZY, orphanRemoval = true )
	private List<DetalleComprobante> detalleordenF;
	
	@OneToOne
	@JoinColumn(name = "idtipocomprobante", nullable = false)
	private TipoComprobante tipocomprobante;

	public int getIdcomprobante() {
		return idcomprobante;
	}

	public void setIdcomprobante(int idcomprobante) {
		this.idcomprobante = idcomprobante;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getConsumidor() {
		return consumidor;
	}

	public void setConsumidor(String consumidor) {
		this.consumidor = consumidor;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getDescuento() {
		return descuento;
	}

	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getSerie() {
		return serie;
	}

	public void setSerie(int serie) {
		this.serie = serie;
	}

	public int getSerieref() {
		return serieref;
	}

	public void setSerieref(int serieref) {
		this.serieref = serieref;
	}

	public LocalDate getFecharef() {
		return fecharef;
	}

	public void setFecharef(LocalDate fecharef) {
		this.fecharef = fecharef;
	}

	public int getTiporef() {
		return tiporef;
	}

	public void setTiporef(int tiporef) {
		this.tiporef = tiporef;
	}

	public int getNumeroref() {
		return numeroref;
	}

	public void setNumeroref(int numeroref) {
		this.numeroref = numeroref;
	}

	public double getTotalref() {
		return totalref;
	}

	public void setTotalref(double totalref) {
		this.totalref = totalref;
	}

	public List<DetalleComprobante> getDetalleordenF() {
		return detalleordenF;
	}

	public void setDetalleordenF(List<DetalleComprobante> detalleordenF) {
		this.detalleordenF = detalleordenF;
	}

	public TipoComprobante getTipocomprobante() {
		return tipocomprobante;
	}

	public void setTipocomprobante(TipoComprobante tipocomprobante) {
		this.tipocomprobante = tipocomprobante;
	}
	
	
	
	
}
