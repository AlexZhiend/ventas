package com.centrum.DAO;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.centrum.Model.Producto;



@Repository
public interface IProductoDAO extends JpaRepository<Producto, Serializable>{

	public Producto findProductoByNombreproducto(String nombreproducto);
	
//	@Query(nativeQuery = true, name = "ReporteProductoGeneral")
//	List<ProductoReport[]> reporteProductoGeneral();
//	
//	@Query(nativeQuery = true, name = "ReporteProductoVenta")
//	List<ProductoVentasReport[]> reporteProductoVentas(@Param("fechainicio") String fechainicio, @Param("fechafin") String fechafin);
}
