package com.centrum.DAO;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.centrum.Model.CategoriaProducto;



@Repository
public interface ICategoriaProductoDAO extends JpaRepository<CategoriaProducto, Serializable>{
	public CategoriaProducto findCategoriaProductoByIdcategoriaproducto(int idcategoriaproducto);

}
