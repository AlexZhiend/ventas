package com.centrum.DAO;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.centrum.Model.TipoComprobante;

@Repository
public interface ITipoComprobanteDAO extends JpaRepository<TipoComprobante, Serializable>{

}
