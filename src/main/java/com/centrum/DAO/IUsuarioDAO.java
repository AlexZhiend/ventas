package com.centrum.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.centrum.Model.Usuario;



public interface IUsuarioDAO extends JpaRepository<Usuario, Long> {
	
	Usuario findOneByUsername(String username);
}