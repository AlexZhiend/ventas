package com.centrum.DAO;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.centrum.Model.DetalleComprobante;


@Repository
public interface IDetalleComprobanteDAO extends JpaRepository<DetalleComprobante, Serializable>{
/*
	@Modifying
	@Query(value = "INSERT INTO detallecomprobante(idcomprobantepago, idexamenmedico) VALUES  (:idcomprobantepago, :idexamenmedico)", nativeQuery = true)
	int registrar(@Param("idcomprobantepago") Integer idcomprobantepago, @Param("idexamenmedico") Integer idexamenmedico); 
	*/
}
