package com.centrum.DAO;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.centrum.Model.Comprobante;



@Repository
public interface IComprobanteDAO extends JpaRepository<Comprobante, Serializable>{

	@Query(value = "select * from comprobante cp where cp.estado=1 and cp.numero = :numero", nativeQuery = true)
	public Comprobante buscarvigentesXnumero(@Param("numero") int numero);
	
	@Query(value = "select * from comprobante cp where cp.estado=1 and cp.idtipocomprobante = :idtipocomprobante order by idcomprobante desc limit 1", nativeQuery = true)
	Comprobante buscarnumero(@Param("idtipocomprobante") int idtipocomprobante);
	
	@Query(value = "select * from ordenfarmacia of where of.estado = 1 order by idordenfarmacia ", nativeQuery = true)
	List<Comprobante> listarvigentes();
	
//	@Query(nativeQuery = true, name = "ReporteOrdenFPaciente")
//	List<OrdenFReport[]> reporteOrdenFPaciente(@Param("numeroorden") int numeroorden);
//	
//	@Query(nativeQuery = true, name = "TotalFarmaciaxDia")
//	TotalxDiaFarmacia totalxdia(@Param("fecha") String fecha);
//	
//	@Query(nativeQuery = true, name = "ReporteOrdenFDetallado")
//	List<DetalladoVentaF[]> detalladoFarmacia(@Param("fechaordenfarmacia") String fechaordenfarmacia);
//	
//	@Query(nativeQuery = true, name = "NotaCreditoDetallado")
//	List<NCreditoFarmaciaReport[]>  NCFarmaciaReport(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin);
}
