package com.centrum.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.centrum.Exception.ModeloNotFoundException;
import com.centrum.Model.Producto;
import com.centrum.service.impl.ProductoServiceImpl;


@RestController
@RequestMapping("/producto")
public class ProductoController {

	@Autowired
	private ProductoServiceImpl productoServiceImpl;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> Registrar(@Valid @RequestBody Producto producto) {
		productoServiceImpl.registrar(producto);
		URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(producto.getNombreproducto()).toUri();
				
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping( consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> ActualizarProducto( @Valid @RequestBody Producto producto) {
		productoServiceImpl.actualizar(producto);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producto>> ListarProducto() {
		List<Producto> productos= new ArrayList<>();
		productos= productoServiceImpl.listar();
		return new ResponseEntity<List<Producto>>(productos, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> BuscarPorID(@PathVariable("id") int id) {
		Producto producto= new Producto();
		producto=productoServiceImpl.buscarPorID(id);
		if (producto==null) {
			throw new ModeloNotFoundException("ID:" + id);
		}
		return new ResponseEntity<Producto>(producto, HttpStatus.OK);
	}

	@GetMapping(value = "nombre/{nombre}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> BuscarPorNombre(@PathVariable("nombre") String nombre) {
		Producto producto= new Producto();
		producto=productoServiceImpl.buscarNombre(nombre);
		if (producto==null) {
			throw new ModeloNotFoundException("nombre:" + nombre);
		}
		return new ResponseEntity<Producto>(producto, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteProducto", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteProductoGeneral() {
		byte[] data = null;
		data = productoServiceImpl.generarReporteProducto();
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteProductoVentas/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteProductoVentas(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = productoServiceImpl.generarReportePventas(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteExcelVentas/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReportePVentasXls(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = productoServiceImpl.generarReporteXLSPventas(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteExcelProducto", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteProductosXls() {
		byte[] data = null;
		data = productoServiceImpl.generarReporteXLSProductos();
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	
}
