package com.centrum.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.centrum.Exception.ModeloNotFoundException;
import com.centrum.Model.Proveedor;
import com.centrum.service.impl.ProveedorServiceImpl;


@RestController
@RequestMapping("/proveedor")
public class ProveedorController {

	@Autowired
	private ProveedorServiceImpl proveedorServiceImpl;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> RegistrarProveedor(@Valid @RequestBody Proveedor proveedor){
			
		proveedorServiceImpl.registrar(proveedor);
		URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(proveedor.getIdproveedor()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> ActualizarProveedor(@RequestBody Proveedor proveedor){

		proveedorServiceImpl.actualizar(proveedor);

		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Proveedor>> ListarProveedor(){
		List<Proveedor> proveedores=new ArrayList<>();
		proveedores = proveedorServiceImpl.listar();

		return new ResponseEntity<List<Proveedor>>(proveedores, HttpStatus.OK);
		
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Proveedor> listarId(@PathVariable("id") Integer id) {
		Proveedor proveedor = new Proveedor();
		proveedor = proveedorServiceImpl.buscarId(id);
		if (proveedor == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<Proveedor>(proveedor, HttpStatus.OK);
	}
	

}
