package com.centrum.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.centrum.Exception.ModeloNotFoundException;
import com.centrum.Model.CategoriaProducto;
import com.centrum.service.impl.CategoriaProductoServiceImpl;


@RestController
@RequestMapping("/categoriaproducto")
public class CategoriaProductoController {

	@Autowired
	private CategoriaProductoServiceImpl categoriaProductoServiceImpl;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> RegistrarCategoriaProducto(@Valid @RequestBody CategoriaProducto categoriaProducto) {
		
		categoriaProductoServiceImpl.registrar(categoriaProducto);
		URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(categoriaProducto.getIdcategoriaproducto()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> ActualizarCategoriaProducto(@Valid @RequestBody CategoriaProducto categoriaProducto) {
		
		categoriaProductoServiceImpl.actualizar(categoriaProducto);
		
		return new ResponseEntity<Object>( HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CategoriaProducto>> ListarCategoriaProducto() {
		List<CategoriaProducto> categoriaProductos =new ArrayList<>();
		categoriaProductos=categoriaProductoServiceImpl.listar();

		return new ResponseEntity<List<CategoriaProducto>>(categoriaProductos, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CategoriaProducto> listarId(@PathVariable("id") Integer id) {
		CategoriaProducto categoriaProducto = new CategoriaProducto();
		categoriaProducto = categoriaProductoServiceImpl.buscarId(id);
		if (categoriaProducto == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<CategoriaProducto>(categoriaProducto, HttpStatus.OK);
	}
	
}
