package com.centrum.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.centrum.Exception.ModeloNotFoundException;
import com.centrum.Model.CategoriaProducto;
import com.centrum.Model.TipoComprobante;
import com.centrum.service.impl.TipoComprobanteServiceImpl;

@RestController
@RequestMapping("/tipocomprobante")
public class TipoComprobanteController {
	
	@Autowired
	private TipoComprobanteServiceImpl tipoComprobanteServiceImpl;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> Registrar(@Valid @RequestBody TipoComprobante tipoComprobante) {
		
		tipoComprobanteServiceImpl.registrar(tipoComprobante);
		URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(tipoComprobante.getIdtipocomprobante()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> Actualizar(@Valid @RequestBody TipoComprobante tipoComprobante) {
		
		tipoComprobanteServiceImpl.actualizar(tipoComprobante);
		
		return new ResponseEntity<Object>( HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TipoComprobante>> Listar() {
		List<TipoComprobante> tipoComprobantes =new ArrayList<>();
		tipoComprobantes=tipoComprobanteServiceImpl.listar();

		return new ResponseEntity<List<TipoComprobante>>(tipoComprobantes, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TipoComprobante> listarId(@PathVariable("id") Integer id) {
		TipoComprobante tipoComprobante = new TipoComprobante();
		tipoComprobante = tipoComprobanteServiceImpl.buscarId(id);
		if (tipoComprobante == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<TipoComprobante>(tipoComprobante, HttpStatus.OK);
	}
	
}
