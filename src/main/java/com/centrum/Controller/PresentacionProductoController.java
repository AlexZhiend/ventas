package com.centrum.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.centrum.Exception.ModeloNotFoundException;
import com.centrum.Model.PresentacionProducto;
import com.centrum.service.impl.PresentacionProductoServiceImpl;


@RestController
@RequestMapping("/presentacionproducto")
public class PresentacionProductoController {

	@Autowired
	private PresentacionProductoServiceImpl presentacionProductoServiceImpl;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> RegistrarPresentacionProducto(@Valid @RequestBody PresentacionProducto presentacionProducto){

		presentacionProductoServiceImpl.registrar(presentacionProducto);
		URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(presentacionProducto.getIdpresentacion()).toUri();
		
		return ResponseEntity.created(location).build();
		
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> ActualizarPresentacionProducto(@Valid @RequestBody PresentacionProducto presentacionProducto) {

		presentacionProductoServiceImpl.actualizar(presentacionProducto);
		System.out.print(presentacionProducto);
		return new ResponseEntity<Object>(HttpStatus.OK);
		
		
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PresentacionProducto>> ListarPresentacionProducto(){
		List<PresentacionProducto> presentacionProductos = new ArrayList<>();
		presentacionProductos=presentacionProductoServiceImpl.listar();

		return new ResponseEntity<List<PresentacionProducto>>(presentacionProductos, HttpStatus.OK);

	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PresentacionProducto> listarId(@PathVariable("id") Integer id) {
		PresentacionProducto presentacionProducto = new PresentacionProducto();
		presentacionProducto = presentacionProductoServiceImpl.buscarId(id);
		if (presentacionProducto == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<PresentacionProducto>(presentacionProducto, HttpStatus.OK);
	}
	
	
}
