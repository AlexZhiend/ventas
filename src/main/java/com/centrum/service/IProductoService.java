package com.centrum.service;

import java.util.List;

import com.centrum.Model.Producto;


public interface IProductoService extends ICRUDService<Producto>{
	Producto buscarNombre(String nombre);
	Producto buscarPorID(int id);
	byte[] generarReporteProducto();
	byte[] generarReportePventas(String fechainicio,String fechafin);
	byte[] generarReporteXLSPventas(String fechainicio,String fechafin);
	byte[] generarReporteXLSProductos();
}
