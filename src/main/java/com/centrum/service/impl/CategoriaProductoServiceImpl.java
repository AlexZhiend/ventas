package com.centrum.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centrum.DAO.ICategoriaProductoDAO;
import com.centrum.Model.CategoriaProducto;
import com.centrum.service.ICategoriaProductoService;


@Service
public class CategoriaProductoServiceImpl implements ICategoriaProductoService{

	@Autowired
	private ICategoriaProductoDAO icategoriaproductodao;

	@Override
	public CategoriaProducto registrar(CategoriaProducto categoriaProducto) {
		// TODO Auto-generated method stub
		return icategoriaproductodao.save(categoriaProducto);
	}

	@Override
	public CategoriaProducto actualizar(CategoriaProducto categoriaProducto) {
		// TODO Auto-generated method stub
		return icategoriaproductodao.save(categoriaProducto);
	}

	@Override
	public List<CategoriaProducto> listar() {
		// TODO Auto-generated method stub
		return icategoriaproductodao.findAll();
	}

	@Override
	public CategoriaProducto buscarId(int id) {
		// TODO Auto-generated method stub
		return icategoriaproductodao.findCategoriaProductoByIdcategoriaproducto(id);
	}

}
