package com.centrum.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centrum.DAO.IProveedorDAO;
import com.centrum.Model.Proveedor;
import com.centrum.service.IProveedorService;


@Service
public class ProveedorServiceImpl implements IProveedorService{
	
	@Autowired
	private IProveedorDAO iproveedordao;

	@Override
	public Proveedor registrar(Proveedor proveedor) {
		// TODO Auto-generated method stub
		return iproveedordao.save(proveedor);
	}

	@Override
	public Proveedor actualizar(Proveedor proveedor) {
		// TODO Auto-generated method stub
		return iproveedordao.save(proveedor);
	}

	@Override
	public List<Proveedor> listar() {
		// TODO Auto-generated method stub
		return iproveedordao.findAll();
	}

	@Override
	public Proveedor buscarId(int id) {
		// TODO Auto-generated method stub
		return iproveedordao.findOne(id);
	}
	

}
