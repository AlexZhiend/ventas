package com.centrum.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centrum.DAO.IPresentacionProductoDAO;
import com.centrum.Model.PresentacionProducto;
import com.centrum.service.IPresentacionProductoService;



@Service
public class PresentacionProductoServiceImpl implements IPresentacionProductoService{
	@Autowired
	private IPresentacionProductoDAO ipresentacionproductodao;

	@Override
	public PresentacionProducto registrar(PresentacionProducto presentacionProducto) {
		// TODO Auto-generated method stub
		return ipresentacionproductodao.save(presentacionProducto);
	}

	@Override
	public PresentacionProducto actualizar(PresentacionProducto presentacionProducto) {
		// TODO Auto-generated method stub
		return ipresentacionproductodao.save(presentacionProducto);
	}

	@Override
	public List<PresentacionProducto> listar() {
		// TODO Auto-generated method stub
		return ipresentacionproductodao.findAll();
	}

	@Override
	public PresentacionProducto buscarId(int id) {
		// TODO Auto-generated method stub
		return ipresentacionproductodao.findOne(id);
	}

}
