package com.centrum.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.centrum.DAO.IProductoDAO;
import com.centrum.Model.Producto;
import com.centrum.service.IProductoService;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.ExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

@Service
public class ProductoServiceImpl implements IProductoService{
	
	@Autowired
	private IProductoDAO iproductodao;

	@Override
	public Producto registrar(Producto producto) {
		// TODO Auto-generated method stub
		return iproductodao.save(producto);
	}

	@Override
	public Producto actualizar(Producto producto) {
		// TODO Auto-generated method stub
		return iproductodao.save(producto);
	}

	@Override
	public Producto buscarId(String identificador) {
		// TODO Auto-generated method stub
		return iproductodao.findProductoByNombreproducto(identificador);
	}

	@Override
	public List<Producto> listar() {
		// TODO Auto-generated method stub
		return iproductodao.findAll();
	}

	@Override
	public Producto buscarNombre(String nombre) {
		// TODO Auto-generated method stub
		return iproductodao.findProductoByNombreproducto(nombre);
	}

	@Override
	public Producto buscarPorID(int id) {
		// TODO Auto-generated method stub
		return iproductodao.findOne(id);
	}
//
//	@Override
//	public byte[] generarReporteProducto() {
//		byte[] data = null;
//		try {
//			File file = new ClassPathResource("/reports/Producto.jasper").getFile();
//			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iproductodao.reporteProductoGeneral()));
//			data = JasperExportManager.exportReportToPdf(print);
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return data;
//	}
//
//	@Override
//	public byte[] generarReportePventas(String fechainicio, String fechafin) {
//		byte[] data = null;
//		try {
//			File file = new ClassPathResource("/reports/ventasmensual.jasper").getFile();
//			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iproductodao.reporteProductoVentas(fechainicio, fechafin)));
//			data = JasperExportManager.exportReportToPdf(print);
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return data;
//	}
//
//	@Override
//	public byte[] generarReporteXLSPventas(String fechainicio, String fechafin) {
//		byte[] data = null;
//		try {
//			
//			File file = new ClassPathResource("/reports/ventasmensual.jasper").getFile();
//			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iproductodao.reporteProductoVentas(fechainicio, fechafin)));
//            
//            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
//            JRXlsxExporter exporter = new JRXlsxExporter();
//            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
//            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
//            exporter.exportReport();
//			
//			data = outputStream1.toByteArray();
//
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return data;
//	}
//	
//	@Override
//	public byte[] generarReporteXLSProductos() {
//		byte[] data = null;
//		try {
//			
//			File file = new ClassPathResource("/reports/Producto.jasper").getFile();
//			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iproductodao.reporteProductoGeneral()));
//			
//            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
//            JRXlsxExporter exporter = new JRXlsxExporter();
//            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
//            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
//            exporter.exportReport();
//			
//			data = outputStream1.toByteArray();
//
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return data;
//	}

	@Override
	public byte[] generarReporteProducto() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] generarReportePventas(String fechainicio, String fechafin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] generarReporteXLSPventas(String fechainicio, String fechafin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] generarReporteXLSProductos() {
		// TODO Auto-generated method stub
		return null;
	}

}
