package com.centrum.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.centrum.DAO.IComprobanteDAO;
import com.centrum.Model.Comprobante;
import com.centrum.service.IComprobanteService;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

@Service
public class ComprobanteServiceImpl implements IComprobanteService {

	@Autowired
	private IComprobanteDAO iComprobanteDAO;

	@Override
	public Comprobante registrar(Comprobante comprobante) {

		comprobante.getDetalleordenF().forEach(x -> x.setComprobante(comprobante));

		return iComprobanteDAO.save(comprobante);
	}

	@Override
	public Comprobante actualizar(Comprobante ordenFarmacia) {

		ordenFarmacia.getDetalleordenF().forEach(x -> x.setComprobante(ordenFarmacia));

		return iComprobanteDAO.save(ordenFarmacia);
	}

	@Override
	public List<Comprobante> listar() {
		return iComprobanteDAO.listarvigentes();
	}

	@Override
	public Comprobante buscarultimo(int idtipocomprobante) {
		// TODO Auto-generated method stub
		return iComprobanteDAO.buscarnumero(idtipocomprobante);
	}

//	@Override
//	public byte[] generarReporteOrdenFarmacia(int numeroorden) {
//		byte[] data = null;
//		try {
//			File file = new ClassPathResource("/reports/ordenfarmacia.jasper").getFile();
//			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iComprobanteDAO.reporteOrdenFPaciente(numeroorden)));
//			data = JasperExportManager.exportReportToPdf(print);
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return data;
//	}
////
//	@Override
//	public TotalxDiaFarmacia totalxdia(String fecha) {
//		// TODO Auto-generated method stub
//		return iComprobanteDAO.totalxdia(fecha);
//	}
//
//	@Override
//	public byte[] generarRDetalladoFarmacia(String fechaordenfarmacia) {
//		byte[] data = null;
//		try {
//			File file = new ClassPathResource("/reports/farmaciadetallado.jasper").getFile();
//			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iOrdenFarmaciaDAO.detalladoFarmacia(fechaordenfarmacia)));
//			data = JasperExportManager.exportReportToPdf(print);
//            
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return data;
//	}
//
//	@Override
//	public byte[] generarRDetalladoFarmaciaXLSX(String fechaordenfarmacia) {
//		byte[] data = null;
//		try {
//			File file = new ClassPathResource("/reports/farmaciadetallado.jasper").getFile();
//			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iOrdenFarmaciaDAO.detalladoFarmacia(fechaordenfarmacia)));
//			
//            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
//            JRXlsxExporter exporter = new JRXlsxExporter();
//            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
//            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
//            exporter.exportReport();
//			
//			data = outputStream1.toByteArray();
//			
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return data;
//	}

	@Override
	public Comprobante buscarVigente(int numeroorden) {
		// TODO Auto-generated method stub
		return iComprobanteDAO.buscarvigentesXnumero(numeroorden);
	}
//
//	@Override
//	public byte[] generarNCfarmaciaReport(String fechainicio, String fechafin) {
//		byte[] data = null;
//		try {
//			File file = new ClassPathResource("/reports/Notas.jasper").getFile();
//			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iOrdenFarmaciaDAO.NCFarmaciaReport(fechainicio, fechafin)));
//			data = JasperExportManager.exportReportToPdf(print);
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return data;
//	}
//
//	@Override
//	public byte[] generarNCfarmaciaReportXLSX(String fechainicio, String fechafin) {
//		byte[] data = null;
//		try {
//			File file = new ClassPathResource("/reports/Notas.jasper").getFile();
//			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iOrdenFarmaciaDAO.NCFarmaciaReport(fechainicio, fechafin)));
//			
//            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
//            JRXlsxExporter exporter = new JRXlsxExporter();
//            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
//            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
//            exporter.exportReport();
//			
//			data = outputStream1.toByteArray();
//			
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return data;
//	}

	@Override
	public byte[] generarReporteOrdenFarmacia(int numeroorden) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] generarRDetalladoFarmacia(String fechaordenfarmacia) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] generarRDetalladoFarmaciaXLSX(String fechaordenfarmacia) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] generarNCfarmaciaReport(String fechainicio, String fechafin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] generarNCfarmaciaReportXLSX(String fechainicio, String fechafin) {
		// TODO Auto-generated method stub
		return null;
	}

}
