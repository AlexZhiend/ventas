package com.centrum.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centrum.DAO.IMenuDAO;
import com.centrum.Model.Menu;
import com.centrum.service.IMenuService;



@Service
public class MenuServiceImpl implements IMenuService{

	@Autowired
	private IMenuDAO iMenuDAO;
	
	@Override
	public void registrar(Menu menu) {
		iMenuDAO.save(menu);
		
	}

	@Override
	public void modificar(Menu menu) {
		iMenuDAO.save(menu);
		
	}

	@Override
	public void eliminar(int idMenu) {
		iMenuDAO.delete(idMenu);
		
	}

	@Override
	public Menu listarId(int idMenu) {
		
		return iMenuDAO.findOne(idMenu);
	}

	@Override
	public List<Menu> listar() {
		
		return iMenuDAO.findAll();
	}

	@Override
	public List<Menu> listarMenuPorUsuario(String nombre) {
		
		List<Menu> menus = new ArrayList<>();
		iMenuDAO.listarMenuPorUsuario(nombre).forEach( x -> {
			Menu m = new Menu();
			m.setIdMenu((Integer.parseInt(String.valueOf(x[0]))));
			m.setIcono(String.valueOf(x[1]));
			m.setNombre(String.valueOf(x[2]));
			m.setUrl(String.valueOf(x[3]));		
	
			menus.add(m);
		});
		return menus;
	}

}
