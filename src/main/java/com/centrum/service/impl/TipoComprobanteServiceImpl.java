package com.centrum.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centrum.DAO.ITipoComprobanteDAO;
import com.centrum.Model.TipoComprobante;
import com.centrum.service.ITipoComprobanteService;

@Service
public class TipoComprobanteServiceImpl implements ITipoComprobanteService{

	@Autowired
	private ITipoComprobanteDAO iTipoComprobanteDAO;
	
	@Override
	public TipoComprobante registrar(TipoComprobante t) {
		// TODO Auto-generated method stub
		return iTipoComprobanteDAO.save(t);
	}

	@Override
	public TipoComprobante actualizar(TipoComprobante t) {
		// TODO Auto-generated method stub
		return iTipoComprobanteDAO.save(t);
	}

	@Override
	public List<TipoComprobante> listar() {
		// TODO Auto-generated method stub
		return iTipoComprobanteDAO.findAll();
	}

	@Override
	public TipoComprobante buscarId(int id) {
		// TODO Auto-generated method stub
		return iTipoComprobanteDAO.findOne(id);
	}

}
