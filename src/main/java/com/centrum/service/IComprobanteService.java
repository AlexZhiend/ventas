package com.centrum.service;

import java.util.List;

import com.centrum.Model.Comprobante;



public interface IComprobanteService{

	Comprobante actualizar(Comprobante ordenFarmacia);
	List<Comprobante> listar();
	Comprobante registrar(Comprobante ordenFarmacia);
	Comprobante buscarultimo(int idtipocomprobante);
	Comprobante buscarVigente(int numeroorden);
	byte[] generarReporteOrdenFarmacia(int numeroorden);
//	TotalxDiaFarmacia totalxdia(String fecha);
	byte[] generarRDetalladoFarmacia(String fechaordenfarmacia);
	byte[] generarRDetalladoFarmaciaXLSX(String fechaordenfarmacia);
	byte[] generarNCfarmaciaReport(String fechainicio,String fechafin);
	byte[] generarNCfarmaciaReportXLSX(String fechainicio,String fechafin);
}
