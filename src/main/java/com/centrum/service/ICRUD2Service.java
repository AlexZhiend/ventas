package com.centrum.service;

import java.util.List;

public interface ICRUD2Service<T> {
	
	T registrar(T t);

	T actualizar(T t);

	List<T> listar();
	
	T buscarId(int id);
}
