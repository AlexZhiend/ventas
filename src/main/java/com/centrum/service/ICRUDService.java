package com.centrum.service;

import java.util.List;

public interface ICRUDService<P> {

	P registrar(P p);
	P actualizar(P p);
	P buscarId(String identificador);
	List<P> listar();
}
